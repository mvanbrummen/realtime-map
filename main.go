package main

import (
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/negroni"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

const staticPath = "./frontend/build/"

var (
	lat  = -34.397
	long = 150.644
)

type Point struct {
	Lat  float64 `json:"lat"`
	Long float64 `json:"long"`
}

func locationHandler(w http.ResponseWriter, r *http.Request) {
	lat = lat + 0.1
	long = long + 0.1

	b, err := json.Marshal(Point{
		Lat:  lat,
		Long: long,
	})

	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/location", locationHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir(staticPath)))

	handler := cors.Default().Handler(r)

	n := negroni.Classic()
	n.UseHandler(handler)

	srv := &http.Server{
		Handler:      n,
		Addr:         "127.0.0.1:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Starting server...")
	log.Fatal(srv.ListenAndServe())
}
