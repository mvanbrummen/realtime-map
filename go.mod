module github.com/mvanbrummen/realtime-map

require (
	github.com/gorilla/mux v1.7.0
	github.com/rs/cors v1.6.0
	github.com/sirupsen/logrus v1.3.0
	github.com/urfave/negroni v1.0.0
)
