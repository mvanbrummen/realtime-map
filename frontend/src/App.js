import React, { Component } from 'react';
import Map from './components/Map';

const apiKey = "inserthere";

class App extends Component {
  state = {
    lat: -34.397,
    long: 150.644
  }

  constructor(props) {
    super(props);

    setInterval(this.tick, 2000)
  }

  tick = () => {
    fetch('http://localhost:8080/location')
      .then(r => r.json())
      .then(r => {
        this.setState({
          lat: r.lat,
          long: r.long
        })
      })
  }

  render() {
    return (
      <>
        <h1>Realtime Map Example</h1>

        <Map
          isMarkerShown
          googleMapURL={"https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=" + apiKey}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `800px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          lat={this.state.lat}
          long={this.state.long}
        />
      </>
    );
  }
}
export default App;
